export const environment = {
  production: false,
  wsTwitchBrotherBack: 'ws://twitch-brother-back.herokuapp.com/ws',
  twitchBrotherDataSnapshot: 'https://twitch-brother-back.herokuapp.com/datasnapshot'
};
